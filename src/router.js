import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/HomePage'
import Work from '@/pages/WorkPage'
import SingleWork from '@/pages/SingleWorkPage'
import Contact from '@/pages/ContactPage'

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/work',
      name: 'Work',
      component: Work
    },
    {
      path: '/work/goldmax',
      name: 'SingleWork',
      component: SingleWork
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }  
})
